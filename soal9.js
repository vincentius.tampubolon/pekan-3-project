const userData = {
    nama: "Farhan",
    user_type: "kalmat",
}

const functionPromise = (userData) => {
    return new Promise((resolve, reject) => {
        if (userData.user_type == 'free') {
            resolve('Maaf kamu tidak punya hak untuk mengakses ini')
        }else if(userData.user_type == 'pro') {
            resolve('Silahkan Masuk dan Buat Produk yang Kamu Inginkan')
        } else {
            reject('User Type tidak diketahui')
        }
    })
}

functionPromise(userData)
    .then(value => {
        console.log(value)
    })
    .catch(value => {
        console.log(value)
    })