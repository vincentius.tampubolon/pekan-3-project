function sumFunction(...nums) {
    let total = 0;
    for (num of nums) {
        total += num;
    }
    return total;
}

console.log(sumFunction(3,5));
console.log(sumFunction(1,3,5,2,4));
console.log(sumFunction(12,7,2,5,1));